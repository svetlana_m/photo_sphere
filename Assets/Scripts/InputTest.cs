﻿using UnityEngine;
using System.Collections;

public class InputTest : MonoBehaviour {
 
	void Update() {
		int fingerCount = 0;
		foreach (Touch touch in Input.touches) {
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
				fingerCount++;

		}
		if (fingerCount > 0) {
			print ("User has " + fingerCount + " finger(s) touching the screen");
			Debug.Log("User has " + fingerCount + " finger(s) touching the screen");
			//Console.WriteLine("User has " + fingerCount + " finger(s) touching the screen");
		}

	}
}